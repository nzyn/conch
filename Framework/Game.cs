using System;
using conch.Framework.GameObjects;

namespace conch.Framework
{
    public class Game : BaseGame
    {
        Board board;

        public Game()
        {
            board = new Board();
        }

        public override void Update()
        {
            Console.ReadKey(true);
        }

        public override void Draw()
        {
            board.Draw();
        }
    }
}