using System;
using conch.Framework;

namespace conch
{
    internal class Runtime
    {
        Game game;

        public Runtime()
        {
            game = new Game();
        }

        internal void Start()
        {
            while (!game.GameIsOver)
            {
                Console.Clear();
                game.Draw();
                game.Update();
            }
        }
    }
}