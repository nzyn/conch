namespace conch.Framework.GameObjects.Base
{
    public abstract class GameObject : IUpdatable
    {
        public abstract void Update();
    }
}