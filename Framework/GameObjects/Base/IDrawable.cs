namespace conch.Framework.GameObjects.Base
{
    public interface IDrawable
    {
        string GFX { get; set; }

         void Draw();
    }
}