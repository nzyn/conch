namespace conch.Framework.GameObjects.Base
{
    public interface IUpdatable
    {
         void Update();
    }
}