namespace conch.Framework.GameObjects.Base
{
    public abstract class DrawableGameObject : GameObject, IDrawable
    {
        public string GFX { get; set; }

        public abstract void Draw();
    }
}