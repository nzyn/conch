namespace conch.Framework
{
    public interface IGame
    {
        void Draw();
        void Update();
    }
}