﻿using System;

namespace conch
{
    class Program
    {
        static void Main(string[] args)
        {
            var runtime = new Runtime();
            runtime.Start();
        }
    }
}
