using System;
using conch.Framework.GameObjects.Base;

namespace conch.Framework.GameObjects
{
    public class Board : DrawableGameObject
    {
        public Board()
        {
            GFX = Graphics.Chess.Board;
        }

        public override void Update()
        {
            
        }

        public override void Draw()
        {
            Console.Write(GFX);
        }
    }
}