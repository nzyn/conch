using System;

namespace conch.Framework
{
    public abstract class BaseGame : IGame
    {
        public bool GameIsOver { get; private set; }

        public abstract void Draw();
        public abstract void Update();

        protected void GameOver()
        {
            GameIsOver = true;
        }
    }
}